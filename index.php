<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="./favicons/favicon.ico">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <title>Dane o wycieczkach</title>            
        <style>
            .footer {
                position: absolute;
                bottom: 0;
                color: #3490dc;
                font-size: 1.2em;
            }

            nav {
                font-size: 1.5em;
                color: #3490dc;
            }

            .bold{
                font-weight: bold;
                margin-top: 2em;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row justify-content-md-center">
                <nav class="col-md-6 text-left">
                    Dane o wycieczkach
                </nav>            
            </div>
            <div class="row justify-content-md-center align-items-center">
                <div class="col-md-8">
                <?php
                    require_once("connect.php");
                    @$connection = new mysqli($dbhost, $dbuser, $dbpass, $dbname);

                    if ($connection->connect_errno) {
                        echo "Nie udało się połączyć z bazą danych :( ! <br>";
                        echo "Błąd: ".$connection->connect_error;
                        exit();
                    }
                    else{
                        try{
                            runModule($connection);                                        
                        }
                        catch(Exception $qe){
                            echo "Nie udało sie odczytac danych z kwerendy.<br>";
                            echo "Błąd: ".$qe->getMessage();
                        }
                        $connection->close();
                    }  

                    function runModule($connection){
                        $selectTripsId = "SELECT `id`,`name`, `measure_interval` AS `s` FROM `trips`";
                        $result;
                        if(!$result = $connection->query($selectTripsId)){
                            throw new  Exception($connection->error);   
                        }
                        $tripsAndSData = createMeasuresArray($result);
                        $tripsData = $tripsAndSData['tripsData'];
                        $tripsNames = $tripsAndSData['tripsNames'];
                        $s = $tripsAndSData['sArray'];
                        $result->free();

                        $selectMeasures = "SELECT `trip_measures`.`id` AS id, `distance`, `trip_id`, `measure_interval`  FROM `trip_measures` INNER JOIN `trips` ON `trip_measures`.`trip_id` = `trips`.`id`";
                        if(!$result = $connection->query($selectMeasures)){
                            throw new  Exception($connection->error);   
                        }
                        $completedTripsData = completeMeasuresArray($result, $tripsData);
                        $result->free();
                                
                        $selectDistances = "SELECT `trips`.`id`, MAX(`distance`) AS `distance` FROM `trips` INNER JOIN `trip_measures` ON `trips`.`id` = `trip_measures`.`trip_id` GROUP BY `trips`.`id`";
                        if(!$result = $connection->query($selectDistances)){
                            throw new Exception($connection->error);   
                        }
                        $distances = getDistances($result);
                        $result->free();

                        $avgSpeed = calculateAverageSpeed($completedTripsData, $s);
                        $maxAvgSpeed = calculateMaxAvgSpeed($avgSpeed);
                        printResult($tripsNames, $distances, $s, $maxAvgSpeed);  
                    }

                    function createMeasuresArray($result){
                        $tripsData;
                        $sArray = array();
                        $names = array();
                        while($row = $result->fetch_assoc()){
                            $tripId = intval($row['id']);
                            $tripsData[$tripId] = array();
                            $sArray[$tripId] = intval($row['s']);
                            $names[$tripId] = $row['name'];
                        }

                        return array(
                            'tripsData' => $tripsData,
                            'sArray' => $sArray,
                            'tripsNames' => $names
                        );
                    }

                    function completeMeasuresArray($result, $array){
                        while($row = $result->fetch_assoc()){
                            $tripId = intval($row['trip_id']);
                            $countMeasures = count($array[$tripId]);
                            $array[$tripId][$countMeasures] = $row['distance'];                      
                        }

                        $counter = array();
                        for($y = 1; $y <= count($array); $y++){
                            $counter[$y] = count($array[$y]);
                        }

                        return array(
                            'measures' => $array,
                            'counter' => $counter,
                        );
                    }

                    function calculateAverageSpeed($data, $sArray){
                        $v = array();
                        $measureArray = $data['measures'];
                        $tripsCounter = $data['counter'];

                        for($y = 1; $y <= count($measureArray); $y++){
                            $measureCount = $tripsCounter[$y];
                            if($measureCount <= 1){
                                $v[$y] = 0;
                            }
                            else{
                                $v[$y] = array();
                                for($x = 1; $x <= $measureCount - 1; $x++){
                                    $delta = $measureArray[$y][$x] - $measureArray[$y][$x - 1];
                                    $v[$y][$x] = (3600 * $delta) / $sArray[$y];
                                }
                            }
                        }

                        return $v;
                    }

                    function calculateMaxAvgSpeed($avgSpeed){
                        $maxAvgSpeed;
                        for($y = 1; $y <= count($avgSpeed); $y++){
                            if(is_array($avgSpeed[$y])){
                                $max = $avgSpeed[$y][1];
                                for($x = 2; $x <= count($avgSpeed[$y]); $x++){
                                    $comaparator = $avgSpeed[$y][$x];
                                    if($max < $comaparator){
                                        $max = $comaparator;
                                    }
                                }
                                $maxAvgSpeed[$y] = $max;
                            }
                            else{
                                $maxAvgSpeed[$y] = 0;
                            }
                            $maxAvgSpeed[$y] = floor($maxAvgSpeed[$y]);
                        }

                        return $maxAvgSpeed;
                    }

                    function getDistances($result){
                        $distanes;
                        while($row = $result->fetch_assoc()){
                            $tripId = intval($row['id']);
                            $distances[$tripId] = $row['distance'];
                        }

                        return $distances;
                    }

                    function printResult($names, $distances, $mInterval, $maxAvg){
                        $divSize = 3;
                        echo '<div class="row bold text-center">';
                        echo '<div class="col-md-'.$divSize.'">Trip</div>';
                        echo '<div class="col-md-'.$divSize.'">Distance</div>';
                        echo '<div class="col-md-'.$divSize.'">Measure Interval</div>';
                        echo '<div class="col-md-'.$divSize.'">Max Average Speed</div>';
                        echo '</div>';
                        for($i = 1; $i <= count($names); $i++){
                            echo '<div class="row text-center">';
                            echo '<div class="col-md-'.$divSize.'">'.$names[$i].'</div>';
                            echo '<div class="col-md-'.$divSize.'">'.$distances[$i].'</div>';
                            echo '<div class="col-md-'.$divSize.'">'.$mInterval[$i].'</div>';
                            echo '<div class="col-md-'.$divSize.'">'.$maxAvg[$i].'</div>';
                            echo "</div>";
                        }
                        
                    }
                ?> 
                </div>
            </div>
            <footer class="row text-center pt-4">
                <div class="col-md-6 footer justify-content-md-center">
                    Marcin Radwan - zadanie rekrutacyjne 2020
                </div>
            </div>
        </div>
    </body>
</html>