# DOKUMENTACJA

## Wykorzystana technologia
Projekt został stworzony przy użyciu języków PHP oraz MySQL, a także została wykorzystana lokalna baza do której dane dostępowe są podane w pliku connect.php

## Uruchomienie projektu
Po przerzuceniu plików na serwer i sporządzeniu bazy, wpisz adres serwera.
Jeżeli posiadasz plik.html, to dodaj do ścieżki /index.php
Sprawdź dane dostępu w pliku connect.php

## Efekt działania
![listing](https://i.imgur.com/3JUEGmJ.png)